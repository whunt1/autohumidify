#include "DHT.h"
#define DHTTYPE DHT11

//Pin numbers
int     DHTPIN                = 3;
int     TEMPERATURE_INDICATOR = 5;
int     LAMP_RELAY;                           // Not yet implemented.
int     HUMIDITY_INDICATOR    = 2;
int     HUMIDIFIER_RELAY      = 7;

//Diagnostics messages
String  START_MESSAGE         = "Starting...  Please wait for first reading...";
String  FAIL_MESSAGE          = "Failed To Read.";
String  TOO_HOT_MESSAGE       = "Too Hot!";
String  TOO_COLD_MESSAGE      = "Too Cold!";
String  TOO_DRY_MESSAGE       = "Too Dry!";
String  HUMIDIFYING_MESSAGE   = "Humidifying...";

//Operational constants
float   MIN_TEMP              = 63;           // minimum safe temp in farenheit
float   MAX_TEMP              = 80;           // maximum safe temp in farenheit
float   MIN_HUMIDITY          = 50;           // minimum safe humidity %
float   DESIRED_HUMIDITY      = 80;           // desired humidity %
int     READING_DELAY         = 1000 * 60;    // time in ms between sensor readings
int     BURST_DURATION        = 1000 * 10;    // duration in ms of water dispensal
int     HOUR_LENGTH           = 60;           // iterations per cycle

//Initialize variables
float   h;                                    // % humidity     
float   f;                                    // farenheit temperature
bool    tooHot;                               // flag for too hot
bool    tooCold;                              // flag for too cold
bool    tooDry;                               // flag for too low humidity
int     minuteCounter;                        // counter for minutes in an hour

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  //Start minute counter at 0
  minuteCounter = 0;

  //Begin serial communications and print start message
  Serial.begin(9600);
  Serial.println(START_MESSAGE);

  //Set pin modes
  pinMode(HUMIDITY_INDICATOR    , OUTPUT);
  pinMode(TEMPERATURE_INDICATOR , OUTPUT);
  pinMode(HUMIDIFIER_RELAY      , OUTPUT);

  //Initialize DHT sensor
  dht.begin();
}

void loop() {

  //Delay before reading
  delay(READING_DELAY);

  //Reset flags, then get readings from sensor and set flags and vars
  resetFlags();
  getReadings();
  if (!goodReadings()){
    Serial.println("Failed to get reading, retrying...");
    return;
  }

  //Print diagnostics and turn on indicator lights if needed
  diagnostic();
  checkVals();
  indicate();

  //Increase minute count by 1
  minuteCounter++;
  
  //On end minute, bring moisture to desired value and reset minute counter
  if(minuteCounter > HOUR_LENGTH){
    bringToDesiredHumidity();
    minuteCounter = 0;
  }
  
 }

/**
 * Print sensor readings to screen.
 */
 void diagnostic(){
  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(f);
  Serial.println(" *F\t");
 }

/**
 * Return true if sensor has retrieved valid readings, false otherwise.
 */
 boolean goodReadings(){
  boolean flag = !(isnan(h) || isnan(f));
  if(!flag)Serial.println(FAIL_MESSAGE); 
  return flag;
 }

/**
 * Set all danger flags to false.
 */
 void resetFlags(){
  tooHot    = false;
  tooCold   = false;
  tooDry    = false;
 }

/**
 * Check sensor vals to set flags.
 */
 void checkVals(){
   if(h < MIN_HUMIDITY) tooDry  = true;
   if(f < MIN_TEMP)     tooCold = true;
   if(f > MAX_TEMP)     tooHot  = true;
 }

/**
 * Update temp and humidity from sensor
 */
 void getReadings(){
  h = dht.readHumidity();
  f = dht.readTemperature(true); 
 }

/**
 * Turn on humidifier for the burst duration
 */
 void humidify(){
  Serial.println( HUMIDIFYING_MESSAGE);
  digitalWrite(HUMIDIFIER_RELAY, LOW);
  Serial.println("Spray start");
  delay(BURST_DURATION);
  Serial.println("Spray stop");
  digitalWrite(HUMIDIFIER_RELAY, HIGH); 
 }

/**
 * Update LED's
 */
 void indicate(){
  if(tooDry){
    digitalWrite(HUMIDITY_INDICATOR, HIGH);  
  }else{
    digitalWrite(HUMIDITY_INDICATOR, LOW);  
  }
  
  if(tooCold || tooHot){
    digitalWrite(TEMPERATURE_INDICATOR, HIGH);  
  }else{
    digitalWrite(TEMPERATURE_INDICATOR, LOW);  
  }
 }

/**
 * Humidify until humidity comes above minimum
 */
void bringToDesiredHumidity(){
  Serial.println("BringtoMin called");
  while(h < MIN_HUMIDITY){
    humidify();
    getReadings();
  }
}
